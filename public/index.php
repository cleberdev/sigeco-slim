<?php declare(strict_types = 1);

namespace Sigeco;

require_once __DIR__ . '/../vendor/autoload.php';

use function Sigeco\Infrastructure\routes;
use DL2\Slim\Application;
use DL2\Slim\Middleware\CORS;
use DL2\Slim\Middleware\UserAgentRequired;
use DL2\Slim\Middleware\JSONP;

header('X-Powered-By: DL2WS', true);

$app = Application::createInstance()
    ->overrideErrorHandler()
    ->overrideNotFoundHandler()
    ->overridePhpErrorHandler()
    ->add(new CORS('*'))
    ->add(new UserAgentRequired())
    ->add(new JSONP())
;

foreach (routes() as $path => $ctrlClass) {
    /** @psalm-suppress ArgumentTypeCoercion */
    $app->use($path, "\\Sigeco\\Actions\\{$ctrlClass}");
}

$app->run();

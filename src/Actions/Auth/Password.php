<?php declare(strict_types = 1);

namespace Sigeco\Actions\Auth;

use DL2\Slim\Exception;
use DL2\Slim\Utils\JWT;
use Sigeco\Actions\Controller;
use Sigeco\Exception\MissingFieldException;
use Slim\Http\Request;
use Slim\Http\Response;

class Password extends Controller
{
    protected const REQUIRES_AUTH = false;

    /**
     * ### Parameters
     *  - confirmPassword: string.
     *  - password: string.
     *  - token: string.
     *
     * ### Response
     */
    public function post(Request $req, Response $res): Response
    {
        /** @var ?string */
        $confirmPassword = $req->getParsedBodyParam('confirmPassword');

        /** @var ?string */
        $password = $req->getParsedBodyParam('password');

        /** @var ?string */
        $token = $req->getParsedBodyParam('token');

        if (!$confirmPassword || !$password || !$token) {
            throw new MissingFieldException(['confirmPassword', 'password', 'token']);
        }

        if ($confirmPassword !== $password) {
            $error = [
                'message' => "Password confirmation doesn't match the password.",
                'type'    => 'bad_request',
            ];

            throw new Exception($error);
        }

        /**
         * payload is created in `ResetPassword::post()`.
         *
         * @var object{type:string,username:string}
         */
        $payload = JWT::decode($token);

        // @todo: update user's password for real
        return $res->withJson([
            'type'     => $payload->type,
            'username' => $payload->username,
        ]);
    }
}

<?php declare(strict_types = 1);

namespace Sigeco\Actions\Auth;

use DL2\Slim\Utils\JWT;
use Sigeco\Actions\Controller;
use Sigeco\Exception\MissingFieldException;
use Slim\Http\Request;
use Slim\Http\Response;

class ResetPassword extends Controller
{
    protected const REQUIRES_AUTH = false;

    /**
     * ### Parameters
     *  - type?: string.
     *  - username: string.
     *
     * ### Response
     */
    public function post(Request $req, Response $res, array $args): Response
    {
        /** @var string */
        $username = $req->getParsedBodyParam('username');

        if (!$username) {
            throw new MissingFieldException(['username']);
        }

        /** @var string */
        $type = $req->getParsedBodyParam('type', 'user');

        // @todo: create the recovery token for real
        $token = JWT::encode([
            'exp'      => \time() + 86400, // expires after 24hrs
            'type'     => $type,
            'username' => $username,
        ]);

        return $res->withJson([
            'token' => $token,
        ]);
    }
}

<?php declare(strict_types = 1);

namespace Sigeco\Actions\Server;

use Sigeco\Actions\Controller;
use Slim\Http\Request;
use Slim\Http\Response;

class DateTime extends Controller
{
    protected const REQUIRES_AUTH = false;

    /**
     * @param array{format:'date'|'time'} $args
     */
    public function get(Request $req, Response $res, array $args): Response
    {
        if ('time' === $args['format']) {
            return $res->withJson(\time());
        }

        return $res->withJson(\date('c'));
    }
}

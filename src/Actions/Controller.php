<?php declare(strict_types = 1);

namespace Sigeco\Actions;

use DL2\Slim;

abstract class Controller extends Slim\Controller
{
    protected function init(): void
    {
        // intentionally left blank
    }
}

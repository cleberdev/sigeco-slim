<?php declare(strict_types = 1);

namespace Sigeco\Infrastructure;

use Zend_Db;
use Zend_Db_Table;

Zend_Db_Table::setDefaultAdapter(
    Zend_Db::factory('PDO_MYSQL', [
        'charset'  => 'utf8',
        'dbname'   => 'Sigue',
        'host'     => \getenv('DATABASE_HOSTNAME') ?: \getenv('RT7_DB_HOSTNAME'),
        'password' => \getenv('DATABASE_PASSWORD') ?: \getenv('RT7_DB_PASSWORD'),
        'profiler' => true,
        'username' => \getenv('DATABASE_USERNAME') ?: \getenv('RT7_DB_USERNAME'),
    ])
);

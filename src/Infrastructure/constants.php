<?php declare(strict_types = 1);

namespace Sigeco\Infrastructure;

\define('Sigeco\\ENVIRONMENT', \getenv('APP_ENV') ?: 'development');

const VERSION = '1.0.0';
